<div align=center><img src="https://images.gitee.com/uploads/images/2021/0317/115853_a747d3f2_8543696.png" width="590" height="212"/></div>

<div align="center">
 
[![](https://img.shields.io/badge/%E9%83%A8%E7%BD%B2%E6%96%87%E6%A1%A3-%E7%82%B9%E5%87%BB%E6%9F%A5%E7%9C%8B-yellow)](https://www.kancloud.cn/wanyuekaiyuan11/wanyue-zhishi/2794476)
[![](https://img.shields.io/badge/QQ%E7%BE%A4-995910672-green)](https://qm.qq.com/cgi-bin/qm/qr?k=JShAyXeoKqg2lWFEUSElxELImhjeMG4y&jump_from=webapi)

 

### 重要！重要！请点击上方按钮，查看“部署文档”！！！



------------------------------------------------------------------------
</div>  

### 【20230203更新说明】升级声网sdk

### 【20220923更新说明】
- 1.修复了新闻列表功能，编译小程序后不显示内容
- 2.重新设计制作了个人中心页面布局
- 3.首页增加广告位图片
.....其他更新内容可以扫描下方二维码查看演示！！！

### 项目说明及部署文档（如果对你有用，请点亮右上角的Star！）
##### <a target="_blank" href="https://www.kancloud.cn/wanyuekaiyuan11/wanyue-zhishi/2794476">部署文档</a>  |  <a target="_blank" href="https://www.kancloud.cn/wanyuekaiyuan11/wanyue-zhishi/2794476">发行步骤</a> | <a target="_blank" href="https://www.kancloud.cn/wanyuekaiyuan11/wanyue-zhishi/2794476">常见问题</a> | <a target="_blank" href="https://www.kancloud.cn/wanyuekaiyuan11/wanyue-zhishi/2794476">升级日志</a>
  

 
 ### 系统演示
 ![输入图片说明](%E6%BC%94%E7%A4%BA%E9%93%BE%E6%8E%A5.png)
 ### 目录结构
 - 前端代码 knowledge_uni_app目录
 - 后台代码 knowledge_admin目录
 
    

 ### Web版地址
 - 教师端地址: <a target="_blank" href="https://demo.sdwanyue.com/teacher">https://demo.sdwanyue.com/teacher</a> 账号:13866666666 密码:123456
 - 后台地址: <a target="_blank" href="https://demo.sdwanyue.com/admin">https://demo.sdwanyue.com/admin</a> 账号: demo 密码: 123456
 - 后台仓库地址: <a target="_blank" href="https://gitee.com/WanYueKeJi/Wanyue-knowledge-payment-admin">点击此处</a>
 
  
 ### 项目介绍 
 万岳知识付费系统打造沉浸式学习体验，提升教学质量，还原真实课堂。知识付费功能包含热门精选、在线直播、付费视频、付费音频、付费阅读等营销功能，实现用户快速裂变。提高用户工作效率和收入是成为知识付费的刚需，可以从海量信息中寻找到适合自身的产品，利用碎片化时间和少许资金就能获得自己需要的信息。
 
 万岳知识付费深刻理解用户诉求，紧盯市场需求。帮助大家低成本高效率体验知识付费平台，以利用互联网让人们生活更美好为使命，精益求精，创新研发，为客户创造更多价值！
 * 所有使用到的框架或者组件都是基于开源项目，代码保证100%开源。
 * 系统功能通用，无论是个人还是企业都可以利用该系统快速搭建一个属于自己的知识付费系统。
 
 系统前端采用uni-app+socket.io+WebRtc核心技术, 接口采用PhalApi框架配合TP5.1框架ThinkCMF,系统功能如下:
 
 
 ### 功能展示
![输入图片说明](knowledge001.png)
![输入图片说明](knowledge002.png)
![输入图片说明](knowledge03.png)
  ### 功能对比表
![输入图片说明](%E7%9F%A5%E8%AF%86%E4%BB%98%E8%B4%B9%E5%8A%9F%E8%83%BD%E8%A1%A8%E6%A0%BC(1).png)
  ### 开源版使用须知
  - 需标注"代码来源于万岳科技开源项目"后即可免费自用运营
  - 前端运营时展示的内容不得使用万岳科技相关信息
  - 允许用于个人学习、教学案例
  - 开源版不得直接倒卖源码
  - 禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负

  ### 说明
   * 如果你想使用功能更完善的知识付费系统，请联系QQ客服: 2770722087 获取专业版
   * 如果您想基于知识付费系统进行定制开发，我们提供有偿定制服务支持！
   * 其他合作模式不限，欢迎来撩！
   * 官网地址：[http://git.sdwanyue.com](http://git.sdwanyue.com)
                    
      
  ### 数据库（免费获取sql脚本）
    
<div style='height: 130px'>
        <img class="kefu_weixin" style="float:left;" src="https://gitee.com/WanYueKeJi/wanyue_education_uniapp/raw/newone/pages/%E5%BC%A0%E7%9A%93%E5%BC%80%E6%BA%90.png" width="602" height="123"/>
        <div style="float:left;">
            <p>QQ：2770722087</p>
          <p>QQ群：995910672</p>
          <p>QQ群：681418688</p>
        </div>
    </div>
    <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=JShAyXeoKqg2lWFEUSElxELImhjeMG4y&jump_from=webapi"><img border="0" src="https://images.gitee.com/uploads/images/2021/0317/100424_072ee536_8543696.png" alt="万岳在线教育讨论群" title="万岳在线教育讨论群"></a> 

  ###  开源交流群【加群回答请填写“gitee知识”】

![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_education_web/raw/master/%E4%B8%87%E5%B2%B3%E7%A7%91%E6%8A%80%E5%BC%80%E6%BA%90%E8%AE%A8%E8%AE%BA10%E7%BE%A4%E7%BE%A4%E8%81%8A%E4%BA%8C%E7%BB%B4%E7%A0%81.png)  ![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_education_web/raw/master/%E4%B8%87%E5%B2%B3%E7%A7%91%E6%8A%80%E5%BC%80%E6%BA%90%E8%AE%A8%E8%AE%BA15%E7%BE%A4%E7%BE%A4%E8%81%8A%E4%BA%8C%E7%BB%B4%E7%A0%81.png)


    